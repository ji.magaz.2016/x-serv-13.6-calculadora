#!/usr/bin/python3
import sys

if len(sys.argv) != 4:
	result = "error: invalid number of argument"
else:
	try:
		sys.argv[2] = float(sys.argv[2])
		sys.argv[3] = float(sys.argv[3])
	except ValueError:
	    sys.exit("error: numbers must be float type")

	oper1 = float(sys.argv[2])
	oper2 = float(sys.argv[3])
	if sys.argv[1] == "sumar":
		result = (oper1 * 10000 + oper2 * 10000) / 10000
	elif sys.argv[1] == "restar":
		result = (oper1 * 10000 - oper2 * 10000) / 10000
	elif sys.argv[1] == "multiplicar":
		result = (oper1 * oper2)
	elif sys.argv[1] == "dividir":
		if oper2 == 0.0:
			result = "error: attempt to divide by zero"
		else:
			result = (oper1 / oper2)
	else:
		result = "error: invalid operation. Try sumar, restar, multiplicar, dividir"
	sys.exit(result)
